exports.constructor = function(socket)
{
 	var client = new Object();

 	client.away = false;
  client.awayMsg = null;
  client.channels = [];
  client.isOp = false;
  client.modes = [];
 	client.nick = null;
 	client.pass = null;
  client.quitMessage = null;
  client.realName = null;
 	client.socket = socket;
	client.user = null;
	client.visible = true;

 	return client;
};
