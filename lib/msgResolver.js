
server = require('../server/server.js');

var clients = [];
var nicks = [];
var channels = [];

  //Função interpretadora dos comandos dados pelo usuário.
  //Por poder modificar coisas como lista de nicks, clientes ou canais (Mudança de nicks, Desconexões, etc) esssas informações são necessárias nos parâmetros.
exports.cmdResolve = function (msg, client, nicksList, clientsList, channelsList){
  clients = clientsList;
  nicks = nicksList;
  channels = channelsList;

  var cmd = msg.split(' ');
  switch(cmd[0])
  {
    case '/away':
      away(cmd, client);
      break;
    case '/back':
      back(cmd, client);
      break;
    case '/join':
      join(cmd, client, clients, nicks, channels);
      break;
    case '/nick':
      nick(cmd, client, nicksList);
      break;
    case '/user':
      user(cmd, client);
      break;
    case '/who':
      who(cmd, client, clientsList);
      break;
    case '/quit':
      quit(cmd, client);
      break;
    case "/die":
      server.die();
      break;
    case "/restart":
      server.restart();
      break;
	case "/privmsg":
	  privmsg(cmd, client);
	  break;
  }
}

function privmsg(cmd, client) {
	var socket = client.socket;
	if(!cmd[1] || !cmd[2]) {
		socket.write("Comando incompleto");
	} else if(cmd[2].charAt(0) != ':') {
		socket.write("Mensagens devem comecar com o caracter ':'");
	} else {
        clients.forEach(function(client1){
            if (client1.nick == cmd[1]) {
				cmd.splice(0,2);
				var texto = cmd.join(" ");
				client1.socket.write("Mensagem privada de " + client.nick+ " " + texto+"\n");
			}
        });
	}
}

function who(cmd, client, clients)
{
  //query for all visible
  if(!cmd[1] || cmd[1] == 0)
  {
    client.write("/who for all visible users:\n");

    for(i = 0; i < clients.length; i++)
    {
      if(clients[i].visible)
      {
        client.write(clients.nick)
      }
    }
  }
  else if(cmd[1] && cmd[1] != 0)
  {
    if(cmd[2] && cmd[2] != 'o')
    {
      socket.write("ERROR: invalid request, try /who <mask> <o>\n");
      return;
    }
    else
    {
      //search for operators
      if(cmd[2] && cmd[2] == 'o')
      {
        client.write("/who for all operators matching mask''" + cmd[2] + "':\n'");

        for(i = 0; i < clients.length; i++)
        {
          if(clients[i].visible && clients[i].isOp && (clients[i].nick.includes(cmd[2]) || clients[i].user.includes(cmd[2])))
          {
            client.write(clients.nick);
          }
        }

      }
      else
      {
        client.write("/who for all users matching mask''" + cmd[2] + "':\n'");

        for(i = 0; i < clients.length; i++)
        {
          if(clients[i].visible && (clients[i].nick.includes(cmd[2]) || clients[i].user.includes(cmd[2])))
          {
            client.write(clients.nick);
          }
        }

      }
    }
  }
  else
  {
    socket.write("ERROR: invalid request, try /who <mask or 0>\n");
    return;
  }
}


//Função responsável por replica ruma mensagem enviada a todos os usuários.
exports.msgResolve = function (msg, client){
  //console.log("msg from");
  broadcastAll("{"+client.nick+"}  "+msg+'\n');
}

//Função que executa o comando "/away", muda o status do usuário para away e coloca uma mensagem como resposta automática para mensagens privadas.
function away(cmd, client){
  var socket = client.socket;

  if(!cmd[1])
  {
    socket.write("ERROR: invalid request, try /away <message>\n");
    return;
  }
  else
  {
    var msg = cmd.slice(1);

    client.away = true;
    client.awayMsg = msg.join(" ");

    socket.write("Your status is set as AWAY: " + client.awayMsg + "\n");
  }
}

//Função que executa o comando "/user", iniciando a conexão do usuário com o servidor. Setando informações como modos e nick.
function user(cmd, client){
  var socket = client.socket;

  if(cmd.length < 4)
  {
    socket.write("ERROR: invalid request, try /user <user> <mode> <realname>\n");
    return;
  }
  else
  {
    client.user = cmd[1];

    if(cmd[2] == 0)
    {
      if(client.modes.indexOf('i') > -1)
      {
        client.modes.splice(client.modes.indexOf('i'), 1);
      }
      //receives wallops
      client.modes.push('w');
    }
    else if(cmd[2] == 8)
    {
      if(client.modes.indexOf('w') > -1)
      {
        client.modes.splice(client.modes.indexOf('w'), 1);
      }
      //invisible
      client.modes.push('i');
    }
    else
    {
      socket.write("ERROR: mode invalid, use 0 for wallops or 8 for invisible.\n");
      return;
    }

    client.realName = cmd.slice(3).join(" ");

    console.log(client.modes);

    socket.write("User executed successfully.\n");
  }
}

//Função que executa o camando "/back", na verdade uma segunda parte do comando "/away".
//Seta o status do usuário como de volta, e remove a mensagem de resposta automática.
function back(cmd, client){
  var socket = client.socket;

  if(cmd[1])
  {
    socket.write("ERROR: invalid request, try /back\n");
    return;
  }
  else
  {
    client.away = false;
    client.awayMsg = null;

    socket.write("Your status is no longer set as AWAY.\n");
  }
}

//Função que executa o comando "/join", faz o usuário se juntar a algum canal específico (ou vários canais). Para que este acompanhe as mensagens.
function join(cmd, client, clients, nicks, channels){
  var socket = client.socket;

  if(!cmd[1]){
    socket.write("ERROR: invalid request, try /join <#channel>\n");
    return;
  }
  else{
    var channelName = cmd.slice(1);

    for(i = 0; i < channels.length; i++){
      //check if channel exists.
      if(channels[i].name == channelName){
        var chn = channels[i];
        client.channels.push(chn);
        channels[i].clients.push(client);
        server.channels=channels;
        client.socket.write("You joined " + chn.name + ".\n");
      }
    }
  }
}


//Função que executa o comando "/nick", muda o nick do usuário, se o novo nickname escolhido esteja disponível (não presente na lista de nicks).
function nick(args, client, nicks){
  var socket = client.socket;

  if(!args[1])
  {
    socket.write("ERROR: invalid request, try /nick <nickname>\n");
    return;
  }
  else if(nicks[args[1]])
  {
    socket.write("ERROR: nickname already in use.\n");
    return;
  }
  else
  {
    //change curent nickname
    if(client.nick)
    {
      delete nicks[client.nick];
    }
    client.nick = args[1];
    nicks[args[1]] = client.nick;
  }
  socket.write("Your nickname was successfully set to " + client.nick + "\n");
}

//Função que executa o comando "/quit", desconecta o usuário.
//Exibindo uma mensagem avisando a saída, como também uma mensagem opcional de despedida escrita pelo usuário.
function quit(args, client) {
  var socket = client.socket;
  if(!args[1]){
    // Remove usuario sem exibir mensagem escrita por ele
    if(broadcast(client.nick + " quits\n", client));
    socket.end();
    remove(client);
  } else {
    // Remove usuario exibindo mensagem escrita por ele
    // Remove palavra QUIT do array
    args.splice(0, 1);
    // Monta a mensagem de saida usando os outros itens do array
    var mesg = args.join(" ");
    broadcast(client.nick + " quits: " + mesg + "\n", client, clients, canal=client.channels);
    socket.end();
    remove(client);
  }
}

//Função para uso interno, remove o nick do usuário recem desconectado da lista de nicks e o usuários da lista de users.
function remove(client) {
  delete nicks[client.nick];
  var index = clients.indexOf(client);
  clients.splice(index, 1);
}

//Função para uso interno, repassa a mensagem do usuário a todos os outros users.
// function broadcast(message, sender) {
//   clients.forEach(function (client) {
//     // Don't want to send it to sender
//     if (client.socket === sender) return;
//     client.socket.write(message);
//   });
//   // Log it to the server output too
//   process.stdout.write(message)
// }

function broadcast(message, sender, clients, canal){
    var channel;
    if(clients == null){
        //show on server
        if(sender != null) sender.socket.write(message);
        process.stdout.write(message);
        return true;
    }else{
      var channel = [];
        if(canal == null) channel = sender.channels;
        else channel = canal;

        clients.forEach(function(client1){

            if (client1 === sender) return;
            for (i = 0; i < sender.channels.length; i++){
                    sender.channels[i].clients.forEach(
                      function(c){
                        c.socket.write(channel[i].name + " | " + message)
                      });
            }
        });

        //show on server
        channel.forEach(function(ch){
          process.stdout.write(ch.name + " | " + message);
        });


        return true;
    }}

    function broadcastAll(message){
        clients.forEach(function(client1){
          client1.socket.write(message);
        });
        process.stdout.write(message);
      return true;
    }

exports.broadcast = broadcast;
exports.broadcastAll = broadcastAll;
