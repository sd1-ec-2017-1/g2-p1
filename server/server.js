net = require('net');

//file system library
fs = require("fs");

//entities
channel = require('../entity/channel.js');
clientOff = require('../entity/clientOffline.js');
clientOn = require('../entity/clientOnline.js');

//resolvers
mr = require('../lib/msgResolver.js');

//lists of objects
var clients = [];
var clientsOffline = [];
var nicks = [];
var channels = [];

//create channel #lobby for tests.
if(channels.length == 0)
{
  var channel = channel.constructor();
  channel.name = "#lobby";

  channels.push(channel);
}

//IRC's defaut port
net.createServer(serverOn).listen(6667);

//friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");

function serverOn(socket)
{
  //create a new client object upon new socket
  var client = clientOn.constructor(socket);

  //put the new client in the clients list
  clients.push(client);

  // IP address for desconnect client
  var hostString = socket.remoteAddress;

  // Verify client in offline clients list
  for (var i=0; i < clientsOffline.length; i++)
    {
      if (clientsOffline[i].socket == socket.remoteAddress)
      { // if true, remove the client from offline list
        clientsOffline.splice(clientsOffline.indexOf(i), 1);
      }
    }


  //handle incoming messages from clients
  socket.on('data', msgResolve);

  function msgResolve(data)
  {
    //trim to remove CRLF
    var msg = data.toString().trim();

    if(msg[0].indexOf("/") == 0)
    {
      mr.cmdResolve(msg, client, nicks, clients, channels);
    }
    else if(msg[0] != '')
    {
      mr.msgResolve(msg, client);
    }
  }

  //remove the client from the list when it leaves
  socket.on('end', socketOff);

  function socketOff()
  {
    var clientOffline = clientOff.constructor();
    if (client.nick !== null)
    {
      
      clientOffline.nick = client.nick;
      clientOffline.user = client.user;
      clientOffline.realname = client.realname;
      clientOffline.socket = hostString;

      // adiciona o cliente a lista de clientes offline
      clientsOffline.push(clientOffline);
    }
    
    clients.splice(clients.indexOf(client), 1);    
    mr.broadcast(client.nick + " left the chat.\n", sender=client, clients=clients, canal=client.channels);   
  }
}

exports.restart = function()
{
  mr.broadcastAll("[SYSTEM]: Restarting server.\n");

  // Asynchronous - Opening File <creating control file>./
  fs.open('/tmp/restart_s', 'w+', function(err, fd) {
     if (err) {
        return console.error(err);
     }
  });
}

exports.die = function()
{
  mr.broadcastAll("[SYSTEM]: Shutting down server.\n");

  // Asynchronous - Opening File <creating control file>
  fs.open('/tmp/die', 'w+', function(err, fd) {
     if (err) {
        return console.error(err);
     }
  });
}

exports.channels = channels;